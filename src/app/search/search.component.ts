import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import Dexie from 'dexie';
import { debounceTime, tap, switchMap, distinctUntilChanged, filter } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { MatAccordion } from '@angular/material/expansion';

const API_KEY = environment.API_KEY;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit{
  @Input() minLengthTerm!: any;
  @ViewChild(MatAccordion) accordion: MatAccordion = new MatAccordion;

  searchMoviesCtrl = new FormControl();
  filteredMovies: any;
  isLoading = false;
  errorMsg!: string;
  selectedMovie: any = "";
  public movieToFind: Movie|null = null;
  public disabledQuestion: boolean = true;
  private db: Dexie;
  public showGenre: boolean = false;
  public showYear: boolean = false;
  public showCountry: boolean = false;
  public showActors: boolean = false;
  public showWriter: boolean = false;
  public success: boolean = false;
  public scoreTotal: number = 0;

  constructor(
    private http: HttpClient
  ) {
    this.db = new Dexie("OMDB2");
    this.db.version(1).stores({
      movie: "Title,Poster,Year"
    });
    this.initQuestion();
    this.accordion.closeAll();
  }

  public onSelected() {
    console.log(this.selectedMovie)
    this.selectedMovie = this.selectedMovie;
    if(this.selectedMovie.imdbID === this.movieToFind!.imdbID) {
      this.success = true;
      this.scoreTotal += this.getScore();
    }
  }

  public displayWith(value: any) {
    return value?.Title;
  }

  public clearSelection() {
    this.selectedMovie = "";
    this.filteredMovies = [];
  }

  public ngOnInit() {
    this.searchMoviesCtrl.valueChanges
      .pipe(
        filter(res => {
          return res !== null && res.length >= this.minLengthTerm
        }),
        distinctUntilChanged(),
        debounceTime(1000),
        tap(() => {
          this.errorMsg = "";
          this.filteredMovies = [];
          this.isLoading = true;
        }),
        switchMap((value: string) => {
          return new Observable((observer) => {
            const cache = this.db.table('movie').orderBy('Title').filter((movie) => {
              return movie.Title.toLowerCase().search(value.toLowerCase()) >= 0;
            }).toArray();

            cache.then((data) => {
              if (data.length > 0) {
                observer.next({Search: data});
              } else {
                this.http.get('http://www.omdbapi.com/?apikey=' + API_KEY + '&s=' + value).subscribe((data: any) => {
                  if (data['Search']) {
                    this.db.table('movie').bulkAdd(data['Search']);
                  }
                  observer.next(data);
                })
              }
            })
          });
        })
      )
      .subscribe((data: any) => {
        this.isLoading = false;
        if (data['Search'] == undefined) {
          this.errorMsg = data['Error'];
          this.filteredMovies = [];
        } else {
          this.errorMsg = "";
          this.filteredMovies = data['Search'];
        }
      });
  }

  /** Init a new question */
  public initQuestion() {
    this.disabledQuestion = true;
    this.showGenre = false;
    this.showActors = false;
    this.showCountry = false;
    this.showWriter = false;
    this.showYear = false;
    this.selectedMovie = null;
    this.accordion.closeAll();
    let randomNumber:number = Math.floor(Math.random() * 2404814);
    let size: number = 7 -String(randomNumber).length;
    let id = "tt"+(String("0").repeat(size))+randomNumber;
    id = "tt0086190";
      this.http.get<Movie>('http://www.omdbapi.com/?i='+id+'&type=movie&apikey=' + API_KEY).subscribe({
        next: (data:any) => {
          if(data.Error) {
            this.initQuestion();
          }
          else if(data.Plot === 'N/A' || data.Type !== 'movie') {
            this.initQuestion();
          }
          this.movieToFind = data
          console.log(this.movieToFind)
          this.disabledQuestion = false;
        }
      });
  }

  /** get current score */
  public getScore(): number {
    let costPts: number = 0;
    let score:number = 10
    if(this.showGenre) costPts += 2;
    if(this.showYear) costPts += 2;
    if(this.showCountry) costPts += 2;
    if(this.showActors) costPts += 2;
    if(this.showWriter) costPts += 2;
    score -= costPts
    if(score === 0) this.selectedMovie = this.movieToFind
    return score;
  }

  /** Restart the game */
  public resetAll() {
    this.scoreTotal = 0;
    this.initQuestion();
  }


}

export interface Movie {
  Title: string,
  Year: string,
  imdbID: string,
  Type: string,
  Poster: string,
  Plot?: string,
  Actors?: string,
  Genre?: string,
  Country?: string,
  Writer?: string
}


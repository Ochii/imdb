import { Component, OnInit, ViewChild } from '@angular/core';
import { SearchComponent } from './search/search.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild(SearchComponent) search!:SearchComponent;
  hiddenAddToHomeScreen = false;
  deferredPrompt: any;
  title = "omdb2";

  ngOnInit() {
    window.addEventListener('beforeinstallprompt', (e) => {
      e.preventDefault();
      this.deferredPrompt = e;
      this.hiddenAddToHomeScreen = false;
    });
  }

  addToHomeScreen() {
    this.hiddenAddToHomeScreen = true;
    this.deferredPrompt.prompt();
    this.deferredPrompt.userChoice.then((choiceResult: { outcome: string; }) => {
      if (choiceResult.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt');
      } else {
        console.log('User dismissed the A2HS prompt');
      }
      this.deferredPrompt = null;
    });
  }

  /** restart the game */
  public onReload() {
    this.search!.resetAll();
  }
}
